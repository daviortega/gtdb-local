import { expect } from 'chai'
import fs from 'fs'
import PouchDB from 'pouchdb';
import rimraf from 'rimraf';
import { Gtdb } from "../src/Gtdb";

const defaults = {
	arTestTaxFile: './data-test/ar_taxonomy.tsv',
	arTestTreeFile: './data-test/ar.tree',
	dbPath: 'gtdb-test',
	testDBName: 'gtdb-test/db'
}

const logLevel = 'silent'

describe('Gtdb', () => {
/* 	before(async function() {
		this.timeout(20000)
		await download()
	}) */
	describe('makeGtdb',() => {
		it('should make the test DB', async function() {
			this.timeout(5000)
			const gtdb = new Gtdb(logLevel)
			const gtdbFile = defaults.arTestTaxFile
			return gtdb
				.changePath(defaults.dbPath)
				.changeGtdbFile(gtdbFile)
				.makeDB(false).then(() => {
					const db = new PouchDB(defaults.testDBName)
					return db.info()
				})
				.then(async (info) => {
					const db = await gtdb.connectDB();
					db.close().then(() => {
						return expect(info.doc_count).gte(100);
					});
				})
		})
	})
	describe('createIndex', () => {
		it('should create the index', async function() {
			this.timeout(60000)
			const expected = [false, false, false, false, false, false, false]
			const gtdb = new Gtdb(logLevel)
			return gtdb.changePath(defaults.dbPath)
				.createIndexes()
				.then(async (msg: any) => {
					const results: boolean[] = []
					msg.forEach((item: string) => {
						results.push(item.match(/created|exists/) === null)
					})
					await gtdb.connectDB().then(async (db) => await db.close())
					return expect(results).eql(expected)
				})
		})
	})
	describe('making searches', () => {
		it('should get all records from Proteobacteria phylum', () => {
			const gtdb = new Gtdb(logLevel)
			return gtdb.changePath(defaults.dbPath)
				.connectDB()
				.then(async (db) => {
					return await db.find({
						selector: {
							p: 'Micrarchaeota'
						},
					})
				})
				.then(async (data: any) => {
					await gtdb.connectDB().then(async (db) => {
						await db.close()
						return expect(data.docs.length).eql(40)
					})
				})
		})
		it('should get all records from Micrarchaeota phylum that are not UBA10214 order', () => {
			const gtdb = new Gtdb(logLevel)
			return gtdb.changePath(defaults.dbPath)
				.connectDB()
				.then(async (db) => {
					return await db.find({
						selector: {
							'$and': [
								{
									p: 'Micrarchaeota'
								},
								{
									'$not': {
										o: 'UBA10214'
									}
								} 
							]
						}
					})
				})
				.then(async (data: any) => {
					await gtdb.connectDB().then(async (db) => { await db.close() })
					return expect(data.docs.length).eql(31)
				})
		})
	})
	describe('selectBalancedSample', () => {
		it('should get 10 genomes matching passed name (not exact)', async function () {
			this.timeout(10000)
			const gtdb = new Gtdb(logLevel)
			const treeString = fs.readFileSync(defaults.arTestTreeFile).toString()
			const numberOfGenomes = 10
			const data = gtdb
				.changePath(defaults.dbPath)
				.selectBalancedSample('p', 'Micrarchaeota', [], numberOfGenomes, false ,treeString)
			return gtdb.connectDB().then((db) => {
				const searchOptions = {
					include_docs: true,
					keys: data
				}
				return db.allDocs(searchOptions).then(async (results: any) => {
					const numbeWithRightPhyla = results.rows.filter((item: any) => item.doc.p === 'Micrarchaeota').length
					await db.close()
					return expect(numbeWithRightPhyla).eq(numberOfGenomes)
				})
			})
		})
		it('should get 3 genomes matching passed name (exact)', async function () {
			this.timeout(10000)
			const gtdb = new Gtdb(logLevel)
			const treeString = fs.readFileSync(defaults.arTestTreeFile).toString()
			const numberOfGenomes = 3
			const rank = 'o'
			const value = 'UBA10214'
			const data = gtdb
				.changePath(defaults.dbPath)
				.selectBalancedSample(rank, value, [], numberOfGenomes, true ,treeString)
			return gtdb.connectDB().then((db) => {
				const searchOptions = {
					include_docs: true,
					keys: data
				}
				return db.allDocs(searchOptions).then((results: any) => {
					const numbeWithRightPhyla = results.rows.filter((item: any) => item.doc[rank] === value).length
					return expect(numbeWithRightPhyla).eq(numberOfGenomes)
				})
			})
		})
		it('should get 2 more genomes including GB_GCA_002778455.1', async function () {
			this.timeout(10000)
			const gtdb = new Gtdb(logLevel)
			const treeString = fs.readFileSync(defaults.arTestTreeFile).toString()
			const numberOfGenomes = 3
			const rank = 'o'
			const value = 'UBA10214'
			const required = ['GB_GCA_002778455.1']
			const data = gtdb
				.changePath(defaults.dbPath)
				.selectBalancedSample(rank, value, required, numberOfGenomes, true ,treeString)
			expect(data).to.include('GB_GCA_002778455.1')
			return gtdb.connectDB().then((db) => {
				const searchOptions = {
					include_docs: true,
					keys: data
				}
				return db.allDocs(searchOptions).then((results: any) => {
					const numbeWithRightPhyla = results.rows.filter((item: any) => item.doc[rank] === value).length
					return expect(numbeWithRightPhyla).eq(numberOfGenomes)
				})
			})
		})
		it('should throw if required pick is not present in tree', async function () {
			this.timeout(10000)
			const gtdb = new Gtdb(logLevel)
			const treeString = fs.readFileSync(defaults.arTestTreeFile).toString()
			const numberOfGenomes = 3
			const rank = 'o'
			const value = 'UBA10214'
			const required = ['GB_GCA_002778455.1', 'GB_GCA_002778455.10']
			expect(() => gtdb
				.changePath(defaults.dbPath)
				.selectBalancedSample(rank, value, required, numberOfGenomes, true ,treeString)
			).to.throw('Genome GB_GCA_002778455.10 passed as required pick was not found in tree')
		})
	})
	after(async function() {
		this.timeout(4000)
		const gtdb = new Gtdb(logLevel)
		await gtdb.changePath(defaults.dbPath)
			.connectDB().then((db) => {
				db.destroy()
			}).then(() => {
				rimraf.sync('gtdb-test')
			})
	})
})
