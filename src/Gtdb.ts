'use strict';

import * as fs from 'fs';
import { Logger, LogLevelDescType } from 'loglevel-colored-prefix';
import PouchDB from 'pouchdb';
import pouchdbFind from 'pouchdb-find';
import { Transform } from 'stream';

PouchDB.plugin(pouchdbFind);

import { IGtdbLocalOptions, IGtdbLocalTaxonomyInfo } from './GtdbInterfaces';
import { SampleTaxonomy } from './SampleTaxonomy';

const userHomePath = process.env.HOME;

const logAlias = {
  debug: 20,
  error: 50,
  fatal: 60,
  info: 30,
  trace: 10,
  warn: 40,
};

class Gtdb {
  private db: any;
  private log: Logger;
  private logLevel: LogLevelDescType;
  private files: { [name: string]: string };
  private name: string;
  private path: string;
  private sampling: SampleTaxonomy;

  constructor(logLevel: LogLevelDescType = 'info') {
    this.path = userHomePath + '/.gtdb-local';
    this.name = this.path + '/db';
    this.logLevel = logLevel;
    this.log = new Logger(this.logLevel);
    this.db = '';
    this.sampling = new SampleTaxonomy(this.logLevel);
    this.files = {
      arTax: this.path + '/data/ar_taxonomy.tsv',
      arTree: this.path + '/data/ar.tree',
      bacTax: this.path + '/data/bac_taxonomy.tsv',
      bacTree: this.path + '/data/bac.tree',
    };
  }

  public async connectDB() {
    await this.loadDB();
    return this.db;
  }

  public changeGtdbFile(newFile: string): Gtdb {
    this.files.bacTax = newFile;
    return this;
  }

  public changePath(newPath: string): Gtdb {
    this.path = newPath;
    this.name = this.path + '/db';
    return this;
  }

  public changeDatabaseName(databaseName: string): Gtdb {
    this.name = databaseName;
    return this;
  }

  public async makeDB(close: boolean = true): Promise<null> {
    const log = this.log.getLogger('Gtdb::makeDB');
    if (fs.existsSync(this.name)) {
      this.db = new PouchDB(this.name);
      try {
        await this.db.destroy();
      } catch (err) {
        throw err;
      }
    } else {
      if (!fs.existsSync(this.path)) {
        fs.mkdirSync(this.path);
      }
    }
    this.db = new PouchDB(this.name);
    return new Promise((resolve, reject) => {
      const parseLine = (line: string): IGtdbLocalTaxonomyInfo => {
        try {
          const fields = line.split('\t');
          const id = fields.shift() || '';
          const tax = fields[0].split(';');
          const d = tax[0].split('__')[1] || '';
          const p = tax[1].split('__')[1] || '';
          const c = tax[2].split('__')[1] || '';
          const o = tax[3].split('__')[1] || '';
          const f = tax[4].split('__')[1] || '';
          const g = tax[5].split('__')[1] || '';
          const s = tax[6].split('__')[1] || '';
          const taxonomy: IGtdbLocalTaxonomyInfo = { _id: id, c, d, f, g, o, p, s };
          return taxonomy;
        } catch (e) {
          log.error(`Line was not parsed successfuly\nline: ${line}\n`);
          throw e;
        }
      };

      let buffer = '';
      const pass = new Transform({
        transform: async (chunk, enc, next) => {
          const dataRaw = buffer + chunk.toString();
          const data = dataRaw.split('\n');
          buffer = data.pop() || '';
          const entries: IGtdbLocalTaxonomyInfo[] = [];
          for (const line of data) {
            entries.push(parseLine(line));
          }
          await this.db.bulkDocs(entries).catch((err: any) => {
            throw err;
          });
          log.info(`Added ${data.length} entries to the DB`);
          next();
        },
      });
      pass.on('finish', async () => {
        log.info(`All set building ${this.name} database`);
        if (close) {
          log.info(`Closing ${this.name} database`);
          await this.db.close().then(() => {
            log.info(`${this.name} closed`);
            resolve();
          });
        }
        resolve();
      });
      const readDatabaseFile = fs.createReadStream(this.files.bacTax);
      readDatabaseFile.pipe(pass);
    });
  }

  /**
   * Creates the indexes on PouchDB
   *
   * @returns
   * @memberof Gtdb
   */
  public async createIndexes() {
    const log = this.log.getLogger('Gtdb::createIndexes');
    await this.loadDB();
    const levels = ['d', 'p', 'c', 'o', 'f', 'g', 's'];
    log.info(`Creating index of every field, this might take a while - hold on`);
    const results: any = [];
    for (const level of levels) {
      await this.db
        .createIndex({
          index: {
            fields: [level],
          },
        })
        .then((status: any) => {
          log.info(`Status for index of ${level}: ${status.result}`);
          results.push(status.result);
        });
    }
    return results;
  }

  /**
   * Select a balanced sample of genomes. Tries to balance selection based on the phylogenetic distribution to avoid oversampling of highly sequenced clades.
   *
   * @param {string} level
   * @param {string} name
   * @param {number} N
   * @param {boolean} [exact=true]
   * @param {string} [tree='']
   * @returns {string[]}
   * @memberof Gtdb
   */
  public selectBalancedSample(
    level: string,
    name: string,
    requiredPicks: string[],
    N: number,
    exact: boolean = true,
    tree: string = '',
  ): string[] {
    const log = this.log.getLogger('Gtdb::selectRandom');
    const nodeName = `${level}__${name}`;
    const treeString = tree === '' ? fs.readFileSync(this.files.bacTree).toString() : tree;
    log.info(`Searching for ${N} genomes under ${nodeName}`);
    return this.sampling
      .loadTree(treeString)
      .setNode(nodeName, exact)
      .setRequiredPicks(requiredPicks)
      .selectBalancedSample(N);
  }

  /**
   * Returns a newick string with the GTDB Bacterial tree
   *
   * @returns {string}
   * @memberof Gtdb
   */
  public getBacteriaTree(): string {
    return fs.readFileSync(this.files.bacTree).toString();
  }

  /**
   * Returns a newick string with the GTDB Archaeal tree
   *
   * @returns {string}
   * @memberof Gtdb
   */
  public getArchaeaTree(): string {
    return fs.readFileSync(this.files.arTree).toString();
  }

  private loadDB() {
    const log = this.log.getLogger('Gtdb::loadDB');
    const loadUp = new Promise((resolve, reject) => {
      if (this.db === '') {
        log.info(`Loading up database ${this.name}...`);
        this.db = new PouchDB(this.name);
        const dataInfo = this.db.info().then((info: any) => {
          log.info(`The ${info.db_name} has ${info.doc_count} records.`);
          resolve(this);
        });
      } else {
        log.info(`Database is ready.`);
        resolve();
      }
    });
    return Promise.resolve(loadUp);
  }
}

export { Gtdb };
