import { Logger, LogLevelDescType } from 'loglevel-colored-prefix';
import { Tree, TreeNode } from 'phylogician-ts';

/**
 * Sample the taxonomy tree for genomes.
 *
 * @export
 * @class SampleTaxonomy
 */
export class SampleTaxonomy {
  private tree: Tree;
  private log: Logger;
  private logLevel: LogLevelDescType;
  private nodesFound: TreeNode[];
  private requiredPicks: number[];

  constructor(logLevel: LogLevelDescType = 'info') {
    this.tree = new Tree(logLevel);
    this.log = new Logger(logLevel);
    this.logLevel = logLevel;
    this.nodesFound = [];
    this.requiredPicks = [];
  }

  /**
   * Load a phylogenetic tree in newick format. Must be compatible with phylogician-ts
   *
   * @param {string} treeString
   * @returns
   * @memberof SampleTaxonomy
   */
  public loadTree(treeString: string): this {
    const log = this.log.getLogger('SampleTaxonomy::loadTree');
    this.tree = new Tree(this.logLevel);
    this.tree.buildTree(treeString);
    log.info(`Tree loaded: ${this.tree.getNumberOfLeafs()} leafs`);
    return this;
  }

  /**
   * Find and set the node of the selection.
   * It will match exactly unless second argument is set to false.
   * Then it will match as RegEx
   *
   * @param {string} name
   * @param {boolean} [exact=true]
   * @memberof SampleTaxonomy
   */
  public setNode(name: string, exact: boolean = true): this {
    const log = this.log.getLogger('SampleTaxonomy::setNode');
    const re = new RegExp(name);
    this.nodesFound = exact ? this.tree.findNodeByExactName(name) : this.tree.findNodeByMatch(re);
    log.info(`Found ${this.nodesFound.length} named ${name}`);
    if (this.logLevel === 'debug' || this.logLevel === 'trace') {
      this.nodesFound.forEach(node => {
        log.debug(`Node #${node.id} named ${node.name}`);
      });
    }
    if (this.nodesFound.length === 0) {
      log.error(`The ${name} is not present in the GTDB taxonomy Tree`);
      throw new Error(`The ${name} is not present in the GTDB taxonomy Tree`);
    }
    return this;
  }

  /**
   * Set a list of genomes that should be in the sample.
   *
   * @param {string[]} names
   * @returns {this}
   * @memberof SampleTaxonomy
   */
  public setRequiredPicks(names: string[]): this {
    names.forEach(name => {
      const node = this.tree.findNodeByExactName(name)[0];
      if (!node) {
        throw new Error(`Genome ${name} passed as required pick was not found in tree`);
      }
      if (node.id) {
        this.requiredPicks.push(node.id);
      }
    });
    return this;
  }

  /**
   * Select N genomes quasi-randomly. It tries to balance the selection.
   *
   * @param {string} name
   * @param {number} N
   * @param {boolean} [exact=true]
   * @returns {string[]}
   * @memberof SampleTaxonomy
   */
  public selectBalancedSample(N: number): string[] {
    const log = this.log.getLogger('SampleTaxonomy::selectGenomes');
    const allLeafs = this.tree.getLeafsIds(this.nodesFound[0].id);
    log.info(`Found ${allLeafs.length}`);
    const selected = this.tree.selectBalancedLeafs(this.requiredPicks, this.nodesFound[0].id, N);
    const results = selected.map(id => this.tree.getNode(id).name);
    return results;
  }
}
