interface IGtdbLocalTaxonomyInfo {
  _id: string;
  d: string;
  p: string;
  c: string;
  o: string;
  f: string;
  g: string;
  s: string;
}

interface IGtdbLocalOptions {
  logLevel?: number;
}

export { IGtdbLocalTaxonomyInfo, IGtdbLocalOptions };
