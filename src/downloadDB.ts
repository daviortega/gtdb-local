import fs from 'fs';
import https from 'https';
import { Logger, LogLevelDescType } from 'loglevel-colored-prefix';

const logger = new Logger('info');
const log = logger.getLogger('downloadDB');

const userHomePath = process.env.HOME;
const gtdbPath = userHomePath + '/.gtdb-local';
if (!fs.existsSync(gtdbPath)) {
  fs.mkdirSync(gtdbPath);
}
const dataPath = gtdbPath + '/data';
if (!fs.existsSync(dataPath)) {
  fs.mkdirSync(dataPath);
}

const download = (force: boolean = false) => {
  log.info('Starting download of Archaea taxonomy');
  const p1 = new Promise((resolveP1, rejectP1) => {
    const arTaxFile = fs.createWriteStream(dataPath + '/ar_taxonomy.tsv');
    arTaxFile.on('finish', () => {
      log.info('Download of Archaea taxonomy completed. Saved.');
      resolveP1();
    });
    https.get(
      'https://data.ace.uq.edu.au/public/gtdb/data/releases/release95/95.0/ar122_taxonomy_r95.tsv',
      response => {
        if (response.statusCode !== 200) {
          log.error('Something went wrong downloading the Archaea taxonomy info.');
          log.error(response.statusMessage);
          rejectP1(response.statusMessage);
        }
        log.info('Download of Archaea taxonomy initiated');
        response.pipe(arTaxFile);
      },
    );
  });

  log.info('Starting download of Bacteria taxonomy');
  const p2 = new Promise((resolveP2, rejectP2) => {
    const bacTaxFile = fs.createWriteStream(dataPath + '/bac_taxonomy.tsv');
    bacTaxFile.on('finish', () => {
      log.info('Download of Bacteria taxonomy completed. Saved.');
      resolveP2();
    });
    https.get(
      'https://data.ace.uq.edu.au/public/gtdb/data/releases/release95/95.0/bac120_taxonomy_r95.tsv',
      response => {
        if (response.statusCode !== 200) {
          log.error('Something went wrong downloading the Bacteria taxonomy info.');
          log.error(response.statusMessage);
          rejectP2(response.statusMessage);
        }
        log.info('Download of Bacteria taxonomy initiated');
        response.pipe(bacTaxFile);
      },
    );
  });

  log.info('Starting download of Archaea tree');
  const p3 = new Promise((resolveP3, rejectP3) => {
    const arTreeFile = fs.createWriteStream(dataPath + '/ar.tree');
    arTreeFile.on('finish', () => {
      log.info('Download of Archaea tree completed. Saved.');
      resolveP3();
    });
    https.get('https://data.ace.uq.edu.au/public/gtdb/data/releases/release95/95.0/ar122_r95.tree', response => {
      if (response.statusCode !== 200) {
        log.error('Something went wrong downloading the Archaea tree.');
        log.error(response.statusMessage);
        rejectP3(response.statusMessage);
        return;
      }
      log.info('Download of Archaea tree initiated');
      response.pipe(arTreeFile);
    });
  });

  log.info('Starting download of Bacteria tree');
  const p4 = new Promise((resolveP4, rejectP4) => {
    const bacTreeFile = fs.createWriteStream(dataPath + '/bac.tree');
    bacTreeFile.on('finish', () => {
      log.info('Download of Bacteria tree completed. Saved.');
      resolveP4();
    });
    https.get('https://data.ace.uq.edu.au/public/gtdb/data/releases/release95/95.0/bac120_r95.tree', response => {
      if (response.statusCode !== 200) {
        log.error('Something went wrong downloading the Bacteria tree.');
        log.error(response.statusMessage);
        rejectP4(response.statusMessage);
      }
      log.info('Download of Bacteria tree initiated');
      response.pipe(bacTreeFile);
    });
  });

  return Promise.all([p1, p2, p3, p4]);
};

export { download };
