#!/usr/bin/env node
// tslint:disable: no-console
'use strict';
import chalk from 'chalk';
import figlet from 'figlet';
import { Logger, LogLevelDescType } from 'loglevel-colored-prefix';
import { download } from './downloadDB';
import { Gtdb } from './Gtdb';

chalk.enabled = true;

// tslint:disable-next-line: no-var-requires
const pkjson = require('../package.json');

const splash = figlet.textSync('GTDB-local', { horizontalLayout: 'fitted' });

console.log(chalk.cyan(splash));
console.log(`\t\t\t\t\t\t            ${chalk.cyan('' + pkjson.version)}`);
console.log(chalk.red('\t\t\t\t\t\t\t\t\t   by Davi Ortega'));

const logger = new Logger('info');
const log = logger.getLogger('setup-gtdb');

const isDBSetup = async (): Promise<boolean> => {
  const gtdb = new Gtdb();
  return gtdb.connectDB().then((db): any => {
    return db.info().then((info: any) => {
      return (info.doc_count || 0) > 0;
    });
  });
};

log.info(`Let's setup GTDB`);

isDBSetup().then(async isIt => {
  log.info(isIt);
  if (isIt) {
    log.warn('There is a database setup already. Skipping setup.');
    log.warn('To force rebuild the database, remove the .gtdb-local from home directory and reinstall.');
    return;
  } else {
    await download().then(() => {
      log.info('Downloading GTDB relevant files');
      log.info('Creating the DB');
      const gtdb = new Gtdb();
      gtdb
        .makeDB(false)
        .then(() => {
          return gtdb.createIndexes();
        })
        .then((msg: any) => {
          const results: boolean[] = [];
          msg.forEach((item: string) => {
            results.push(item.match(/created|exists/) === null);
          });
          log.info(`All set, thanks for using gtdb-local`);
        });
    });
  }
});
